1) By default it uses root/1234 credentials fro DB. You can override it by passing properties -Ddbuser=... -Ddbpassword=...
2) Application will try to use MYSQL LOAD DATA INFILE if you server is configured for it. Otherwise it will fallback to batch insert, which is MUCH SLOWER
Examples:

java -cp "parser.jar" com.ef.Parser --accesslog=../access.log --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500
java -Ddbuser=root -Dpassword=1234 -cp "parser.jar" com.ef.Parser --accesslog=../access.log --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500

4) How to build
mvn clean install