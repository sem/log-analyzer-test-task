package com.sem.utils;

import org.knowm.yank.PropertiesUtils;
import org.knowm.yank.Yank;

import java.util.List;
import java.util.Properties;

public class DbHelper {

    public static final String DBUSER = "dbuser";
    public static final String DBPASSWORD = "dbpassword";
    public static final String SQL_DB_PROPERTIES = "sql/db.properties";
    public static final String SQL_STATEMENTS_PROPERTIES = "sql/statements.properties";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    public static void init(){
        String dbUser = System.getProperty(DBUSER);
        String dbPassword = System.getProperty(DBPASSWORD);
        Properties dbProps = PropertiesUtils.getPropertiesFromClasspath(SQL_DB_PROPERTIES);
        Properties sqlProps = PropertiesUtils.getPropertiesFromClasspath(SQL_STATEMENTS_PROPERTIES);
        //override properties
        if (dbUser != null) dbProps.setProperty(USERNAME, dbUser);
        if (dbPassword != null) dbProps.setProperty(PASSWORD, dbPassword);
        Yank.setThrowWrappedExceptions(true);
        Yank.setupDefaultConnectionPool(dbProps);
        Yank.addSQLStatements(sqlProps);
    }

    public static int[] executeBatchSqlKey(String sql, Object [][] args){
        return Yank.executeBatchSQLKey(sql, args);
    }

    public static <T> List<T> queryColumnSQLKey(String sql, String columnName, Class<T> type, Object [] args){
        return Yank.queryColumnSQLKey(sql, columnName, type, args);
    }

    public static int executeSqlKey(String sql, Object [] args){
        return Yank.executeSQLKey(sql, args);
    }

    public static int execute(String sql, Object [] args){
        return Yank.execute(sql, args);
    }

    public static void release(){
        Yank.releaseDefaultConnectionPool();
    }

}
