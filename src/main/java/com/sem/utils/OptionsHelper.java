package com.sem.utils;

import org.apache.commons.cli.*;

import java.util.HashMap;
import java.util.Map;

public class OptionsHelper {

    private Options getOptionsConfiguration(Map<String, String> cmdOptsDefinition) {
        Options options = new Options();
        cmdOptsDefinition.entrySet().forEach(e -> {
            Option opt = Option.builder(e.getKey()).desc(e.getValue()).required().hasArg().longOpt(e.getKey()).build();
            options.addOption(opt);
        });
        return options;
    }

    private CommandLine getCmdLine(String [] args, Map<String, String> cmdOptsDefinition) throws ParseException {
        Options options = getOptionsConfiguration(cmdOptsDefinition);

        CommandLineParser parser = new DefaultParser();
        return parser.parse(options, args);
    }

    public static CommandLine getCommandLine(String [] args, Map<String, String> cmdOptsDefinition) throws ParseException {
        OptionsHelper optionsHelper = new OptionsHelper();
        return optionsHelper.getCmdLine(args, cmdOptsDefinition);
    }


}
