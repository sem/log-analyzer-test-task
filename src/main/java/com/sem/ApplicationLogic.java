package com.sem;

import com.sem.service.DataLoadingService;
import com.sem.service.IpQueryService;
import com.sem.utils.DbHelper;
import com.sem.utils.OptionsHelper;
import lombok.extern.log4j.Log4j;
import org.apache.commons.cli.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Log4j
public class ApplicationLogic {

    public static final String ACCESSLOG = "accesslog";
    public static final String START_DATE = "startDate";
    public static final String DURATION = "duration";
    public static final String THRESHOLD = "threshold";

    private Map<String, String> cmdOptsDefinition = new HashMap<>();
    {
        cmdOptsDefinition.put(ACCESSLOG, "path to accesslog");
        cmdOptsDefinition.put(START_DATE, "start filtering date");
        cmdOptsDefinition.put(DURATION, "hourly, daily");
        cmdOptsDefinition.put(THRESHOLD, "any integer");
    }

    private void doLogic(String[] args) throws Exception {
        CommandLine cmd = OptionsHelper.getCommandLine(args, cmdOptsDefinition);

        String accessLog = cmd.getOptionValue(ACCESSLOG);
        String startDateStr = cmd.getOptionValue(START_DATE);
        String duration = cmd.getOptionValue(DURATION);
        int threshold = Integer.parseInt(cmd.getOptionValue(THRESHOLD));

        DataLoadingService dl = new DataLoadingService();
        log.info("Wiping DB...");
        dl.wipeDB();

        log.info("Loading data into DB...");
        dl.loadData(accessLog);

        log.info("Querying tables...");
        IpQueryService ipQueryService = new IpQueryService();
        List<String> ips = ipQueryService.queryForIps(startDateStr, duration, threshold);
        ips.forEach(System.out::println);
        System.out.println("Count: " + ips.size());

        log.info("Inserting into another table...");
        ipQueryService.insertToAnotherTable(startDateStr, duration, threshold);

        log.info("Done");
    }

    public void run(String[] args) {
        try {
            DbHelper.init();
            doLogic(args);
        } catch (Exception exc){
            log.error("Can't execute application", exc);
        } finally {
            DbHelper.release();
        }
    }

}
