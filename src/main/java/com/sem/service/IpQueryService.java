package com.sem.service;

import com.sem.utils.DbHelper;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class IpQueryService {

    public static final String FIND_IPS = "FIND_IPS";
    public static final String INSERT_FOUND_IPS = "INSERT_FOUND_IPS";

    public static final String DATE_OPTION_FORMAT = "yyyy-MM-dd.HH:mm:ss";
    public static final String HOURLY = "hourly";
    public static final String DAILY = "daily";

    public List<String> queryForIps(String startDateStr, String duration, int threshold) throws ParseException {
        List<Object> parameters = getParametersToFindIps(startDateStr, duration, threshold);
        return DbHelper.queryColumnSQLKey(FIND_IPS, "ip", String.class, parameters.toArray());
    }

    public void insertToAnotherTable(String startDateStr, String duration, int threshold) throws ParseException {
        List<Object> parameters = getParametersToFindIps(startDateStr, duration, threshold);
        DbHelper.executeSqlKey(INSERT_FOUND_IPS, parameters.toArray());
    }

    private List<Object> getParametersToFindIps(String startDateStr, String duration, int threshold) throws ParseException {
        List<Object> parameters = new ArrayList<>();
        Date startDate = DateUtils.parseDate(startDateStr, DATE_OPTION_FORMAT);
        Date endDate = getEndDate(duration, startDate);
        parameters.add(startDate);
        parameters.add(endDate);
        parameters.add(threshold);
        return parameters;
    }

    private Date getEndDate(String duration, Date startDate) {
        Date endDate;
        if(HOURLY.equalsIgnoreCase(duration)){
            endDate = DateUtils.addHours(startDate, 1);
        } else if(DAILY.equalsIgnoreCase(duration)){
            endDate = DateUtils.addDays(startDate, 1);
        } else {
            throw new IllegalArgumentException("Duration is nor hourly neither daily");
        } return endDate;
    }

}
