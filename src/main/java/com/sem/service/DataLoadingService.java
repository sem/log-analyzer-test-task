package com.sem.service;

import com.sem.utils.DbHelper;
import lombok.extern.log4j.Log4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.time.DateUtils;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log4j
public class DataLoadingService {

    public static final Object[] EMPTY_ARRAY = {};
    public static final String WIPE_ACCESS = "WIPE_ACCESS";
    public static final String WIPE_BLOCKED_IPS = "WIPE_BLOCKED_IPS";

    public static final String DATE_PTRN = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String INSERT_ACCESS = "INSERT_ACCESS";
    public static final String LOAD_FAST = "LOAD_FAST";

    public static final int BATCH_SIZE = 1000;

    public void wipeDB() {
        DbHelper.executeSqlKey(WIPE_ACCESS, EMPTY_ARRAY);
        DbHelper.executeSqlKey(WIPE_BLOCKED_IPS, EMPTY_ARRAY);
    }

    public void loadData(String accessLog) throws Exception {
        try {
            loadDataFast(accessLog);
        } catch (Exception exc){
            log.warn("Couldn't load file with LOAD DATA INFILE due to secure-file-priv configuration, fallback to batch loading, it will take much more time");
            loadDataBatch(accessLog);
        }
    }

    private void loadDataFast(String accessLog) throws Exception {
        File file = new File(accessLog);
        DbHelper.executeSqlKey(LOAD_FAST, new String[] { file.getAbsolutePath() });
    }

    private void loadDataBatch(String accessLog) throws Exception {

        try (LineIterator it = FileUtils.lineIterator(new File(accessLog), "UTF-8");) {
            List<Object[]> batch = new ArrayList<>();
            while (it.hasNext()) {
                String line = it.nextLine();
                Object[] parameters = getParametersArray(line);
                if(parameters != null) batch.add(parameters);
                if(batch.size() > BATCH_SIZE){
                    loadBatch(batch);
                    batch.clear();
                }
            }
            if(batch.size() > 0){
                loadBatch(batch);
                batch.clear();
            }
        }

    }

    private void loadBatch(List<Object[]> batch) {
        Object[][] btchy = new Object[][]{};
        btchy = batch.toArray(btchy);
        DbHelper.executeBatchSqlKey(INSERT_ACCESS, btchy);
    }

    private Object[] getParametersArray(String line) throws ParseException {
        try {
            String[] fields = line.split("\\|");
            Object[] parameters = Arrays.copyOf(fields, fields.length, Object[].class);
            parameters[0] = DateUtils.parseDate(fields[0], DATE_PTRN);
            parameters[3] = Integer.parseInt(fields[3]);
            return parameters;
        } catch (Exception exc){
            log.warn("Skipping, cannot parse line:'" + exc.getMessage() + "', line=" + line);
            return null;
        }
    }

}
