DROP SCHEMA testtask;

CREATE SCHEMA testtask;

USE testtask;

CREATE TABLE `access` (
  `request_date` DATETIME,
  `ip` TEXT,
  `request` TEXT,
  `http_status` INT,
  `user_agent` TEXT,
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
);

CREATE TABLE `blocked_ips` (
  `ip` varchar(16) NOT NULL,
  `reason` TEXT,
  PRIMARY KEY (`ip`)
);